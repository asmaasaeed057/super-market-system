from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class email(models.Model):
    _inherit = 'account.invoice'



    @api.multi
    def send_mail_template(self):
        template = self.env.ref('super_market.email_template_edi_invoice_v8')
        self.env['mail.template'].browse(template.id).send_mail(self.id, force_send=True)



