from dateutil.relativedelta import relativedelta

from dateutil.relativedelta import relativedelta

from odoo import models, fields, api,tools

class SalesPerson(models.Model):
    _name = 'super.market.sales_person'
    _inherit = 'res.partner'

    image = fields.Binary()
    birthday = fields.Date(string="DOB")
    age = fields.Integer(string="Age")
    gender = fields.Selection([
        ('male', "Male"),
        ('female', "Female"),
    ])
    marital_status = fields.Selection([
        ('single', "Single"),
        ('married', "Married"),
    ])

    @api.multi
    @api.onchange('birthday')
    def _compute_age(self):
        for record in self:
            if record.birthday and record.birthday <= fields.Date.today():
                record.age = relativedelta(
                    fields.Date.from_string(fields.Date.today()),
                    fields.Date.from_string(record.birthday)).years
            else:
                record.age = 0

