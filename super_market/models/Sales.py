from odoo import models, fields, api,tools
import logging
_logger = logging.getLogger(__name__)

class Sales(models.Model):
    _name = 'super.market.sales'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    sales_person = fields.Many2one('super.market.sales_person', string="Sales person")
    product_id = fields.Many2one('account.invoice', string='product', ondelete='cascade')
